# Getting Started
---

### Before you begin
1. Locate your Secret Key and Partner Id under `Settings` tab in the partner dashboard
2. For testing purposes use the `Test Keys` and for production use the `Production Keys`.

### Accessing The Environments
There is both a Test and Production environment for this API.
Each one has its respective domain name. Use the following host
name to access testing and production:

```
TEST_HOST = api.staging.payhippo.ng
PRODUCTION_HOST = api.payhippo.ng
```

### Authentication Flow
1. Make a `POST` request to the `/auth` endpoint

**Example:**
```
curl -d '{"partnerId":"VALID_ID", "env":"TEST", "secretKey":"VALID_KEY"}' -H "Content-Type: application/json" -X POST https://${HOST}/v1/auth

```
2. An auth `TOKEN` will be provided to be used when making further requests to the api

**Example:**
```
curl -H 'Accept: application/json' -H "Authorization: Bearer ${TOKEN}" https://${HOST}/v1/leads

```

3. Make sure to add `https://` to the host. You will get errors otherwise.

**Example:**

```
curl "https://${HOST}/v1/leads"
```

### Using the API
Browse the reference section of this site to see examples of what you can do with this API and how to use it. You can use the **Try this API** tool on the right side of an API method page to generate a sample request.


## Legacy Auth Flow - API Keys

### Making Authenticated Requests
1. Supply the API Key using a query parameter named `key`.

**Example:**

```
curl "https://${HOST}/v1/leads?key=${API_KEY}"
```